import {
  hourSchema,
  hoursSchema,
  openingHoursSchema,
} from "../src/validators/openingHours";

import { openingHours, openingHoursV2 } from "./constants";

import { State, WeekDay, OpeningHours } from "../src/models/models";

describe("hourSchema", () => {
  it("Validate successfully for type in [open, close] and 0 < value < 86399", () => {
    return expect(
      hourSchema.validateAsync({ type: "close", value: 3600 })
    ).resolves.toStrictEqual({ type: "close", value: 3600 });
  });

  it("Validate successfully for type in [open, close]", () => {
    return expect(
      hourSchema.validateAsync({ type: "open", value: 3600 })
    ).resolves.toStrictEqual({ type: "open", value: 3600 });
  });

  it("Validate successfully for and value = 0", () => {
    return expect(
      hourSchema.validateAsync({ type: "open", value: 0 })
    ).resolves.toStrictEqual({ type: "open", value: 0 });
  });

  it("Validate successfully for and value = 86399", () => {
    return expect(
      hourSchema.validateAsync({ type: "open", value: 86399 })
    ).resolves.toStrictEqual({ type: "open", value: 86399 });
  });

  it("Throw error for empty obj", () => {
    return expect(hourSchema.validateAsync({})).rejects.toThrowError();
  });
  it("Throw error for empty obj", () => {
    return expect(hourSchema.validateAsync({})).rejects.toThrowError();
  });

  it("Throw error for missing type key", () => {
    return expect(
      hourSchema.validateAsync({ value: 3600 })
    ).rejects.toThrowError();
  });

  it("Throw error for missing value key", () => {
    return expect(
      hourSchema.validateAsync({ type: "close" })
    ).rejects.toThrowError();
  });

  it("Throw error for not int value", () => {
    return expect(
      hourSchema.validateAsync({ type: "close", value: 3600.7 })
    ).rejects.toThrowError();
  });

  it("Throw error for not number value", () => {
    return expect(
      hourSchema.validateAsync({ type: "close", value: "ciao" })
    ).rejects.toThrowError();
  });

  it("Throw error for too large value", () => {
    return expect(
      hourSchema.validateAsync({ type: "close", value: 86400 })
    ).rejects.toThrowError();
  });

  it("Throw error for negative value", () => {
    return expect(
      hourSchema.validateAsync({ type: "close", value: -1 })
    ).rejects.toThrowError();
  });

  it("Throw error for type not to be open or close", () => {
    return expect(
      hourSchema.validateAsync({ type: "pending", value: 3600 })
    ).rejects.toThrowError();
  });

  it("Throw error for property not defined", () => {
    return expect(
      hourSchema.validateAsync({ type: "open", value: 3600, test: "test" })
    ).rejects.toThrowError();
  });
});

describe("hoursSchema", () => {
  it("Validate successfully hours schema with one object", () => {
    return expect(
      hoursSchema.validateAsync([{ type: "close", value: 3600 }])
    ).resolves.toStrictEqual([{ type: "close", value: 3600 }]);
  });

  it("Validate successfully hours schema with empty array", () => {
    return expect(hoursSchema.validateAsync([])).resolves.toStrictEqual([]);
  });

  it("Throw error for element different from hour schema", () => {
    return expect(hoursSchema.validateAsync([null])).rejects.toThrowError();
  });

  it("Throw error for element different from hour schema", () => {
    return expect(
      hoursSchema.validateAsync([
        { type: "close", value: 3600 },
        { test: "ciao" },
      ])
    ).rejects.toThrowError();
  });
});

describe("openingHoursSchema", () => {
  it("Validate successfully openingHoursSchema", () => {
    return expect(
      openingHoursSchema.validateAsync({
        monday: [],
        tuesday: [],
        wednesday: [{ type: "close", value: 3600 }],
        thursday: [],
        friday: [],
        saturday: [],
        sunday: [],
      })
    ).resolves.toStrictEqual({
      monday: [],
      tuesday: [],
      wednesday: [{ type: "close", value: 3600 }],
      thursday: [],
      friday: [],
      saturday: [],
      sunday: [],
    });
  });

  it("Validate successfully hours schema with empty array", () => {
    return expect(hoursSchema.validateAsync([])).resolves.toStrictEqual([]);
  });

  it("Throw error for missing thursday in the schema", () => {
    return expect(
      openingHoursSchema.validateAsync({
        monday: [],
        tuesday: [],
        wednesday: [{ type: "close", value: 3600 }],
        friday: [],
        saturday: [],
        sunday: [],
      })
    ).rejects.toThrowError();
  });

  it("Throw error for missing monday in not an array", () => {
    return expect(
      openingHoursSchema.validateAsync({
        monday: null,
        tuesday: [],
        wednesday: [{ type: "close", value: 3600 }],
        friday: [],
        thursday: [],
        saturday: [],
        sunday: [],
      })
    ).rejects.toThrowError();
  });

  it("Throw error for missing monday in not an array", () => {
    return expect(
      openingHoursSchema.validateAsync({
        monday: "test",
        tuesday: [],
        wednesday: [{ type: "close", value: 3600 }],
        friday: [],
        thursday: [],
        saturday: [],
        sunday: [],
      })
    ).rejects.toThrowError();
  });

  it("Throw error for missing monday in not an array", () => {
    return expect(
      openingHoursSchema.validateAsync({
        monday: { type: "close", value: 3600 },
        tuesday: [],
        wednesday: [{ type: "close", value: 3600 }],
        friday: [],
        thursday: [],
        saturday: [],
        sunday: [],
      })
    ).rejects.toThrowError();
  });
});
