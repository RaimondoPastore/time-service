import {
  WeekDay,
  State,
  Time,
  OpeningHours,
  OrderedWeekDays,
  Entries,
} from "./models/models";

const isEqualDay = (day: WeekDay) => (el: WeekDay) => el === day;

export const getDayAfter = (day: WeekDay): WeekDay => {
  const i = OrderedWeekDays.findIndex(isEqualDay(day));
  return OrderedWeekDays[(i + 1) % 7];
};

export const parseTime = (unixTime: number): string => {
  const date = new Date(unixTime * 1000);

  const h = date.getUTCHours();
  const m = date.getUTCMinutes();

  return (
    (h % 12 || 12) +
    (m ? "." + (m < 10 ? "0" + m : m) : "") +
    " " +
    (h < 12 ? "AM" : "PM")
  );
};

export const isClosed = (hours: Array<Time>) =>
  hours.length === 0 || (hours.length === 1 && hours[0].type === State.Close);

export const getTomorrowClosingTime = (
  weekDayMap: OpeningHours,
  day: WeekDay
) => {
  const dayAfter = weekDayMap[getDayAfter(day)];
  const [closingTime]: Time[] = dayAfter;

  if (!closingTime || closingTime.type !== State.Close)
    throw Error(`There is no closing time for ${day}`);

  return closingTime.value;
};

export const parseHours = (
  openingHours: OpeningHours,
  day: WeekDay,
  hours: Array<Time>
): Array<[string, string]> => {
  let prev = null;
  let curr = null;
  let pos = 1;

  const newHours = [];
  for (const el of hours) {
    curr = el;

    if (prev?.type === curr.type) throw Error("Wrong consecutive types");

    if (prev?.type === State.Open && curr.type === State.Close) {
      newHours.push([prev.value, curr.value]);
    }

    if (hours.length === pos && curr.type === State.Open) {
      newHours.push([curr.value, getTomorrowClosingTime(openingHours, day)]);
    }
    prev = curr;
    pos++;
  }

  return newHours.map(([open, close]) => [parseTime(open), parseTime(close)]);
};

export const parseDay = (openingHours: OpeningHours) => ([day, hours]: [
  WeekDay,
  Array<Time>
]): [WeekDay, Array<[string, string]>] => [
  day,
  isClosed(hours) ? [] : parseHours(openingHours, day, hours),
];

export const capitalizeFirst = (word: string) =>
  word[0].toUpperCase() + word.substring(1, word.length);

export const timesToString = (times: Array<[string, string]>) =>
  times.length ? times.map((t) => t.join(" - ")).join(", ") : "Closed";

export const openingHoursToString = (
  tuples: Array<[WeekDay, Array<[string, string]>]>
): string =>
  tuples
    .map(([day, times]) => capitalizeFirst(day) + ": " + timesToString(times))
    .join("\n");

export const logParsed = (input: Array<[WeekDay, Array<[string, string]>]>) => {
  console.log();
  console.log(openingHoursToString(input));
};

export const openingHoursToEntries = (openingHours: OpeningHours) =>
  Object.entries(openingHours) as Entries<OpeningHours>;
