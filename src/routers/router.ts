import { Router, Request, Response, NextFunction } from "express";
import { reduce, map, tap } from "ramda";
import { parseDay, logParsed, openingHoursToEntries } from "../helpers";
import { OpeningHours } from "../models/models";
import {
  openingHoursSchema,
  getMessagesFromError,
} from "../validators/openingHours";

const validateSchemaBody = (req: Request, res: Response, next: NextFunction) =>
  openingHoursSchema
    .validateAsync(req.body)
    .then(() => next())
    .catch((err) =>
      res.status(400).send({ errors: getMessagesFromError(err) })
    );

export const router: Router = Router();

router.post("/", validateSchemaBody, (req, res) => {
  const openingHours = req.body as OpeningHours;

  return Promise.resolve(openingHours)
    .then(openingHoursToEntries)
    .then(map(parseDay(openingHours)))
    .then(tap(logParsed))
    .then(reduce((acc, [day, hours]) => ({ ...acc, [day]: hours }), {}))
    .then((json) => res.send(json))
    .catch((err) => res.status(400).send({ errors: [err.message] }));
});
