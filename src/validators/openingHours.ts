import Joi from "joi";
import { State } from "../models/models";

import { pipe, pluck, pathOr } from "ramda";

const MIN_TIME = 0; //Represent 12 AM
const MAX_TIME = 86399; //Represent 11.59 PM

export const hourSchema = Joi.object({
  type: Joi.string().valid(State.Open, State.Close).required(),
  value: Joi.number().integer().min(MIN_TIME).max(MAX_TIME).required(),
});

export const hoursSchema = Joi.array().items(hourSchema);

export const openingHoursSchema = Joi.object({
  monday: hoursSchema.required(),
  tuesday: hoursSchema.required(),
  wednesday: hoursSchema.required(),
  thursday: hoursSchema.required(),
  friday: hoursSchema.required(),
  saturday: hoursSchema.required(),
  sunday: hoursSchema.required(),
});

export const getMessagesFromError = (err: any) =>
  pipe(pathOr([], ["details"]), pluck("message"))(err);
