import { OpeningHours, State, WeekDay } from "../src/models/models";

export const openingHours: OpeningHours = {
  [WeekDay.Monday]: [],
  [WeekDay.Tuesday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Wednesday]: [],
  [WeekDay.Thursday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Friday]: [
    {
      type: State.Open,
      value: 36000,
    },
  ],
  [WeekDay.Saturday]: [
    {
      type: State.Close,
      value: 4000,
    },
    {
      type: State.Open,
      value: 36000,
    },
  ],
  [WeekDay.Sunday]: [
    {
      type: State.Close,
      value: 3600,
    },
    {
      type: State.Open,
      value: 43200,
    },
    {
      type: State.Close,
      value: 75600,
    },
  ],
};

export const openingHoursV2: OpeningHours = {
  [WeekDay.Monday]: [],
  [WeekDay.Tuesday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Wednesday]: [],
  [WeekDay.Thursday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Friday]: [
    {
      type: State.Open,
      value: 64800,
    },
  ],
  [WeekDay.Saturday]: [
    {
      type: State.Close,
      value: 3600,
    },
    {
      type: State.Open,
      value: 32400,
    },
    {
      type: State.Close,
      value: 39600,
    },
    {
      type: State.Open,
      value: 57600,
    },
    {
      type: State.Close,
      value: 82800,
    },
  ],
  [WeekDay.Sunday]: [
    {
      type: State.Close,
      value: 3600,
    },
    {
      type: State.Open,
      value: 43200,
    },
    {
      type: State.Close,
      value: 75600,
    },
  ],
};

export const openingHoursV3: OpeningHours = {
  [WeekDay.Monday]: [
    {
      type: State.Close,
      value: 5400,
    },
  ],
  [WeekDay.Tuesday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Wednesday]: [],
  [WeekDay.Thursday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Friday]: [
    {
      type: State.Open,
      value: 64800,
    },
  ],
  [WeekDay.Saturday]: [
    {
      type: State.Close,
      value: 3600,
    },
    {
      type: State.Open,
      value: 32400,
    },
    {
      type: State.Close,
      value: 39600,
    },
    {
      type: State.Open,
      value: 57600,
    },
    {
      type: State.Close,
      value: 82800,
    },
  ],
  [WeekDay.Sunday]: [
    {
      type: State.Close,
      value: 3600,
    },
    {
      type: State.Open,
      value: 43200,
    },
    {
      type: State.Close,
      value: 75600,
    },
    {
      type: State.Open,
      value: 77400,
    },
  ],
};

export const openingHoursNotClosing: OpeningHours = {
  [WeekDay.Monday]: [],
  [WeekDay.Tuesday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Wednesday]: [],
  [WeekDay.Thursday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Friday]: [
    {
      type: State.Open,
      value: 36000,
    },
  ],
  [WeekDay.Saturday]: [
    {
      type: State.Open,
      value: 36000,
    },
  ],
  [WeekDay.Sunday]: [
    {
      type: State.Close,
      value: 3600,
    },
    {
      type: State.Open,
      value: 43200,
    },
    {
      type: State.Close,
      value: 75600,
    },
  ],
};

export const openingHoursWithMissingThursday = {
  [WeekDay.Monday]: [],
  [WeekDay.Tuesday]: [
    {
      type: State.Open,
      value: 36000,
    },
    {
      type: State.Close,
      value: 64800,
    },
  ],
  [WeekDay.Wednesday]: [],
  [WeekDay.Friday]: [
    {
      type: State.Open,
      value: 36000,
    },
  ],
  [WeekDay.Saturday]: [
    {
      type: State.Open,
      value: 36000,
    },
  ],
  [WeekDay.Sunday]: [
    {
      type: State.Close,
      value: 3600,
    },
    {
      type: State.Open,
      value: 43200,
    },
    {
      type: State.Close,
      value: 75600,
    },
  ],
};

export const openingHoursIncorrectTimes = (state: State): OpeningHours => ({
  [WeekDay.Monday]: [
    {
      type: state,
      value: 36000,
    },
    {
      type: state,
      value: 64800,
    },
  ],
  [WeekDay.Tuesday]: [],
  [WeekDay.Wednesday]: [],
  [WeekDay.Thursday]: [],
  [WeekDay.Friday]: [],
  [WeekDay.Saturday]: [],
  [WeekDay.Sunday]: [],
});
