

## Opening Hours Service

The service provides an endpoint that accepts opening hours data as an input (JSON) and returns
a more human-readable version of the data formatted using 12-hour clock.

The dedicated endpoint is

```
POST /opening-hours
```

In case you send a correct json body :
- the expected formatted string will be logged
- you will receive a json response 

In case of this input for example:

```json

{
  "monday": [],
  "tuesday": [{"type": "open", "value": 36000}, {"type": "close", "value": 64800}],
  "wednesday": [],
  "thursday": [{"type": "open", "value": 36000}, {"type": "close", "value": 64800}],
  "friday": [{"type": "open", "value": 36000}],
  "saturday": [{"type": "close", "value": 3600}, {"type": "open", "value": 36000}],
  "sunday": [{"type": "close", "value": 3600}, {"type": "open", "value": 43200}, 
             {"type": "close", "value": 75600}]
}
```

you will receive this json:

```json
{
  "monday": [],
  "tuesday": [["10 AM", "6 PM"]],
  "wednesday": [],
  "thursday": [["10 AM", "6 PM"]],
  "friday": [["10 AM", "1.06 AM"]],
  "saturday": [["10 AM", "1 AM"]],
  "sunday": [["12 PM", "9 PM"]]
}
```

and this will be the output from the console

```
Monday: Closed
Tuesday: 10 AM - 6 PM
Wednesday: Closed
Thursday: 10 AM - 6 PM
Friday: 10 AM - 1 AM
Saturday: 10 AM - 1 AM
Sunday: 12 PM - 9 PM
```

### Semantic Validation

- If a restaurant is not closed during the same day, it's required that the closing time is present in the following day.
  If this condition is not satisfied, a Bad request code will be returned. It's not possible to represent opening hours 
  longer than 2 days. 
- If the array that represents the opening and closing time will have 2 consecutive opening or closing time a Bad request 
  code will be returned.

## How to run the application

### Run with npm

```shell
npm i
npm run dev
```

When you run the application locally, the service will be accessible at `localhost:3000`


### Run with docker-compose 

docker and docker-compose are required

```shell
docker-compose up --build
```

### Change default port

The default port is `3000`. To change the port: 
- with npm: `export SERVICE_PORT=3005 && npm run dev`
- with docker: change `SERVICE_PORT` in `.env` file



### Run test and test coverage

```shell
npm test
npm run coverage
```

The docker image is already running them during the build stage.

## Some (Design / Implementation) Choices

### Typescript

Not being an expert in TS, I have tried my best to embrace the typing and the language philosophy in general (but not always).
In some case I have used more javascript approach to solve some problems (see body request validation). 
I had some trouble using some JS libraries in TS (for example Ramda or openapi validator).

### The validation of the body request 

**[Joi](https://github.com/sideway/joi) is the option I chose**.
  
I tried to use:
  - **openapi-express-validator**: I have also created a feature branch but it was not working as expected.
  - **Types in Typescript**: Due to my lack of knowledge in the language I was not able to find an elegant solution.
    Moreover the libraries that I have found were using Typescript experimental feature for decorators (not suitable for production maybe).
  - **Joi**: In the end, I came to the conclusion that Joi is the fastest and most elegant way to implement the body validation 
    (not sure it's the preferred solution for Typescript project).  

###  Testing libraries

- **Jest**
- **Supertest**



## Part 2: Tell us what do you think about the data format. 
```
Is the current JSON structure the best way to store that kind of data or can you come up with a better version?
```

As for all the problems, the solution depends on the use case.
So if I should design the json data structure only for the use case of the exercise, 
I would represent the **Opening Hours** as a list:

```
LIST<OpeningHour>

OpeningHour =  

{
 open :  { day : <WeeyDay>, time: <integer> },
 close : { day : <WeeyDay>, time: <integer> }
}

WeekDay =  Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday

```

The time is associated with the day of opening or closing. 
So no need to have logic to check the next days to get the closing time.

If a week day is not present in any of the open entry, that day is considered closed.

For example a json as the following:

```javascript

[
 {
  open :  { day : "tuesday", time: 36000 },
  close : { day: "tuesday", time: 64800 }
 },
 {
  open :  { day : "thursday", time: 3600 },
  close : { day: "thursday", time: 32400 }
 },
 {
  open :  { day : "thursday", time: 39600 },
  close : { day: "thursday", time: 57600 }
 },
 {
  open :  { day : "friday", time: 36000 },
  close : { day: "saturday", time: 3600 }
 },
 {
  open :  { day : "saturday", time: 36000 },
  close : { day: "sunday", time: 64800 }
 },
 {
  open :  { day : "sunday", time: 43200 },
  close : { day: "sunday", time: 75600 }
 }
]
```

It will have the following result: 

```

Monday: Closed
Tuesday: 10 AM - 6 PM
Wednesday: Closed
Thursday: 9 AM - 11 AM, 4 PM - 11 PM
Friday: 10 AM - 1 AM
Saturday: 10 AM - 1 AM
Sunday: 12 PM - 9 PM
```
