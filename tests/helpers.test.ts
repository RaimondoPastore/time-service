import {
  getTomorrowClosingTime,
  getDayAfter,
  parseTime,
  capitalizeFirst,
  openingHoursToString,
  parseDay,
  openingHoursToEntries,
} from "../src/helpers";

import { openingHours, openingHoursV2, openingHoursV3 } from "./constants";

import { State, WeekDay, OpeningHours } from "../src/models/models";

describe("getDayAfter", () => {
  it("Return the right day", () => {
    expect(getDayAfter(WeekDay.Friday)).toBe(WeekDay.Saturday);
    expect(getDayAfter(WeekDay.Sunday)).toBe(WeekDay.Monday);
  });
});

describe("capitalizeFirst", () => {
  it("Return the capitalize word", () => {
    expect(capitalizeFirst(WeekDay.Friday)).toBe("Friday");
    expect(capitalizeFirst(WeekDay.Sunday)).toBe("Sunday");
  });
});

describe("openingHoursToString", () => {
  it("Return the correct string", () => {
    expect(
      openingHoursToString(
        openingHoursToEntries(openingHours).map(parseDay(openingHours))
      )
    ).toStrictEqual(
      "Monday: Closed\n" +
        "Tuesday: 10 AM - 6 PM\n" +
        "Wednesday: Closed\n" +
        "Thursday: 10 AM - 6 PM\n" +
        "Friday: 10 AM - 1.06 AM\n" +
        "Saturday: 10 AM - 1 AM\n" +
        "Sunday: 12 PM - 9 PM"
    );

    expect(
      openingHoursToString(
        openingHoursToEntries(openingHoursV2).map(parseDay(openingHoursV2))
      )
    ).toStrictEqual(
      "Monday: Closed\n" +
        "Tuesday: 10 AM - 6 PM\n" +
        "Wednesday: Closed\n" +
        "Thursday: 10 AM - 6 PM\n" +
        "Friday: 6 PM - 1 AM\n" +
        "Saturday: 9 AM - 11 AM, 4 PM - 11 PM\n" +
        "Sunday: 12 PM - 9 PM"
    );

    expect(
      openingHoursToString(
        openingHoursToEntries(openingHoursV3).map(parseDay(openingHoursV3))
      )
    ).toStrictEqual(
      "Monday: Closed\n" +
        "Tuesday: 10 AM - 6 PM\n" +
        "Wednesday: Closed\n" +
        "Thursday: 10 AM - 6 PM\n" +
        "Friday: 6 PM - 1 AM\n" +
        "Saturday: 9 AM - 11 AM, 4 PM - 11 PM\n" +
        "Sunday: 12 PM - 9 PM, 9.30 PM - 1.30 AM"
    );
  });
});

describe("parseTime", () => {
  it("Return from unix time the relative AM time", () => {
    expect(parseTime(0)).toBe("12 AM");
    expect(parseTime(3600)).toBe("1 AM");
    expect(parseTime(3660)).toBe("1.01 AM");
    expect(parseTime(37800)).toBe("10.30 AM");
    expect(parseTime(43199)).toBe("11.59 AM");
  });

  it("Return from unix time the relative PM time", () => {
    expect(parseTime(43200)).toBe("12 PM");
    expect(parseTime(45000)).toBe("12.30 PM");
    expect(parseTime(75600)).toBe("9 PM");
    expect(parseTime(75660)).toBe("9.01 PM");
    expect(parseTime(86399)).toBe("11.59 PM");
  });
});

describe("getTomorrowClosingTime", () => {
  it("Return the right closing time", () => {
    expect(getTomorrowClosingTime(openingHours, WeekDay.Friday)).toBe(4000);
    expect(getTomorrowClosingTime(openingHours, WeekDay.Saturday)).toBe(3600);
  });

  it("Throw error if there is no closing time", () => {
    expect(() => {
      getTomorrowClosingTime(openingHours, WeekDay.Monday);
    }).toThrowError(`There is no closing time for monday`);

    expect(() => {
      getTomorrowClosingTime(openingHours, WeekDay.Sunday);
    }).toThrowError(`There is no closing time for sunday`);
  });
});
