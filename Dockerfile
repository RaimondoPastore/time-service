# First stage: build and test
FROM node:alpine as BUILD

WORKDIR /app
COPY . .

RUN npm install && \
    npm run build && \
    npm run test && \
    npm run coverage

# Second stage: assemble the runtime image
FROM node:alpine as RUN

WORKDIR app

COPY --from=BUILD /app/dist/src/ .

COPY package*.json ./
RUN npm install --only=prod

CMD node /app/index.js