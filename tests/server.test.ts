import {
  openingHours,
  openingHoursIncorrectTimes,
  openingHoursNotClosing,
  openingHoursV2,
  openingHoursV3,
  openingHoursWithMissingThursday,
} from "./constants";

import server from "../src/server";
import { State } from "../src/models/models";

const supertest = require("supertest");

const app = server();

test("GET /health", async () => {
  await supertest(app)
    .get("/health")
    .expect(200)
    .then(({ text }: any) => expect(text).toBe("ok"));
});

describe("POST /opening-hours", () => {
  describe("Successfully Requests", () => {
    it("Ok request for valid body", async () => {
      await supertest(app)
        .post("/opening-hours")
        .send(openingHours)
        .expect(200)
        .then(({ body }: any) =>
          expect(body).toStrictEqual({
            monday: [],
            tuesday: [["10 AM", "6 PM"]],
            wednesday: [],
            thursday: [["10 AM", "6 PM"]],
            friday: [["10 AM", "1.06 AM"]],
            saturday: [["10 AM", "1 AM"]],
            sunday: [["12 PM", "9 PM"]],
          })
        );
    });

    it("Ok request for valid body", async () => {
      await supertest(app)
        .post("/opening-hours")
        .send(openingHoursV2)
        .expect(200)
        .then(({ body }: any) =>
          expect(body).toStrictEqual({
            monday: [],
            tuesday: [["10 AM", "6 PM"]],
            wednesday: [],
            thursday: [["10 AM", "6 PM"]],
            friday: [["6 PM", "1 AM"]],
            saturday: [
              ["9 AM", "11 AM"],
              ["4 PM", "11 PM"],
            ],
            sunday: [["12 PM", "9 PM"]],
          })
        );
    });

    it("Ok request for valid body", async () => {
      await supertest(app)
        .post("/opening-hours")
        .send(openingHoursV3)
        .expect(200)
        .then(({ body }: any) =>
          expect(body).toStrictEqual({
            monday: [],
            tuesday: [["10 AM", "6 PM"]],
            wednesday: [],
            thursday: [["10 AM", "6 PM"]],
            friday: [["6 PM", "1 AM"]],
            saturday: [
              ["9 AM", "11 AM"],
              ["4 PM", "11 PM"],
            ],
            sunday: [
              ["12 PM", "9 PM"],
              ["9.30 PM", "1.30 AM"],
            ],
          })
        );
    });
  });

  describe("Semantic validation", () => {
    it("Bad request for not closing time present", async () => {
      await supertest(app)
        .post("/opening-hours")
        .send(openingHoursNotClosing)
        .expect(400)
        .then(({ body }: any) =>
          expect(body).toStrictEqual({
            errors: ["There is no closing time for friday"],
          })
        );
    });

    it("Bad request in case there are 2 hours consecutive that are close", async () => {
      await supertest(app)
        .post("/opening-hours")
        .send(openingHoursIncorrectTimes(State.Close))
        .expect(400)
        .then(({ body }: any) =>
          expect(body).toStrictEqual({
            errors: ["Wrong consecutive types"],
          })
        );
    });

    it("Bad request in case there are 2 hours consecutive that are open", async () => {
      await supertest(app)
        .post("/opening-hours")
        .send(openingHoursIncorrectTimes(State.Open))
        .expect(400)
        .then(({ body }: any) =>
          expect(body).toStrictEqual({
            errors: ["Wrong consecutive types"],
          })
        );
    });
  });
});

describe("Schema validation", () => {
  it("Bad request in case the body is missing a day", async () => {
    await supertest(app)
      .post("/opening-hours")
      .send(openingHoursWithMissingThursday)
      .expect(400)
      .then(({ body }: any) =>
        expect(body).toStrictEqual({
          errors: ['"thursday" is required'],
        })
      );
  });
});
