import server from "./server";

const app = server();

const SERVICE_PORT = process.env.SERVICE_PORT || 3000;

app.listen(SERVICE_PORT, () => {
  console.log(`The application is listening on port ${SERVICE_PORT}!`);
});
