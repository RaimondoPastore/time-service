export enum WeekDay {
  Monday = "monday",
  Tuesday = "tuesday",
  Wednesday = "wednesday",
  Thursday = "thursday",
  Friday = "friday",
  Saturday = "saturday",
  Sunday = "sunday",
}

export enum State {
  Open = "open",
  Close = "close",
}

export interface OpeningHours {
  [WeekDay.Monday]: Time[];
  [WeekDay.Tuesday]: Time[];
  [WeekDay.Wednesday]: Time[];
  [WeekDay.Thursday]: Time[];
  [WeekDay.Friday]: Time[];
  [WeekDay.Saturday]: Time[];
  [WeekDay.Sunday]: Time[];
}

export interface Time {
  type: State;
  value: number;
}

export const OrderedWeekDays: WeekDay[] = [
  WeekDay.Monday,
  WeekDay.Tuesday,
  WeekDay.Wednesday,
  WeekDay.Thursday,
  WeekDay.Friday,
  WeekDay.Saturday,
  WeekDay.Sunday,
];

export type Entries<T> = {
  [K in keyof T]: [K, T[K]];
}[keyof T][];
