import express from "express";
import bodyParser from "body-parser";

import { router } from "./routers/router";

export default () => {
  const app = express();
  app.use(bodyParser.json()); // limit 100kb

  app.use("/opening-hours", router);
  app.get("/health", (req, res) => {
    res.send("ok");
  });

  return app;
};
